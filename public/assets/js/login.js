const loginForm = document.querySelector('#loginUser');

loginForm.addEventListener('submit',(err) => {
	err.preventDefault();

const email = document.querySelector('#userEmail').value
const password = document.querySelector('#password').value


// input fields should not be blank
	if (email == "" || password == "") {
		Swal.fire({
                icon:'warning',
                iconColor: 'orange',
                title: 'Please, Take it easy!',
                text: "Please input your email and/or password.",
                textColor: '#fff',
                background: '#ffe600f6'

            })
	} else {
		fetch('https://fast-fjord-11707.herokuapp.com/api/users/login', {
		method: 'POST',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
		email: email,
		password: password
		})
	}).then(res=>{
		return res.json()
	}).then(data =>{
		console.log(data.access)
		if(data.access){

			// lets create a checker if may nakukuhang data
			// console.log(data)
		localStorage.setItem('token', data.access)
		// this local storage can be found inside the subfolder of appdata of the local file of the google chrome browser inside the data module of the user
		Swal.fire({
                icon:'success',
                iconColor: 'green',
                title: 'Pretty Amazing!',
                text: "Successfully Log in.",
                background: '#ffe600f6',
                showConfirmButton: false,
                timer: 2500
            })
		fetch(`https://fast-fjord-11707.herokuapp.com/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${data.access}`
			}
		}).then(res => {
			return res.json()
		}).then(data => {
			// console.log(data)
			localStorage.setItem('id', data._id)
			localStorage.setItem('isAdmin', data.isAdmin)
			// console.log('items are set inside the local storage')
			// we are trying to direct to the course page upon successful login
			window.location.replace('./profile.html')
		})
	}else{
		Swal.fire({
                icon:'error',
                iconColor: 'red',
                title: 'Awwww, Snap',
                text: "Something went wrong, check your credentials.", 
                background: '#ffe600f6'
            })
	}

	})
	}

})


