// console.log('hello from index.js')
// lets capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession')
let userToken = localStorage.getItem('token')
// console.log(userToken)

// lets create acontrol structure that will determine which elements inside the nav bar will be displayed if the user token is found in the local storgae

if (!userToken) {
	navItem.innerHTML = `<li class="nav-item">
<a href= "./pages/login.html" class="nav-link">Log in</a>
</li>
`
} else {
	navItem.innerHTML = `<li class="nav-item">
<a href= "./pages/logout.html" class="nav-link">Log out</a>
</li>
`
}
