let params = new URLSearchParams(window.location.search)
let id = params.get('courseId');

let token = localStorage.getItem('token')
 
let name = document.querySelector('#courseName')
let desc = document.querySelector('#courseDesc')
let price = document.querySelector('#coursePrice')
let enrollBtn = document.querySelector('#enrollmentContainer')


fetch(`https://fast-fjord-11707.herokuapp.com/api/course/${id}`).then(res => res.json()).then(data => {
name.innerHTML = data.name
desc.innerHTML = data.description
price.innerHTML = data.price
enrollBtn.innerHTML = `<a id = "enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

document.querySelector('#enrollButton').addEventListener('click', () =>{
	
	fetch(`https://fast-fjord-11707.herokuapp.com/api/users/enroll`, {
		
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` 
		},
		body: JSON.stringify({
			userId: token,
			courseId: id 
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		if (data === true) {
			Swal.fire({
	        icon: 'success',
	        iconColor: 'green',
	        title: 'Yey! Succesfully Enrolled!!',
	        background: '#ffe600f6',
	        showConfirmButton: false,
 			timer: 2000
    }).then(() => {window.location.replace("./courses.html")})
	
		} else {
			Swal.fire({
	        icon: 'error',
	        iconColor: 'red',
	        title: 'Oops, Something went wrong',
	        background: '#ffe600f6',
    }); 
		}
	})
})
}) 
