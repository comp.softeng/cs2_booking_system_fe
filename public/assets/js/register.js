// Insert a new User
let registerUserForm = document.querySelector("#registerUser");

registerUserForm.addEventListener("submit", (event) =>{

    event.preventDefault();
    // lets capture values inside the input field
    let fName = document.querySelector("#firstName").value
            // console.log(fName)
    let lName = document.querySelector("#lastName").value
            // console.log(lName)
    let email = document.querySelector("#userEmail").value
            // console.log(email)
    let mobileNo = document.querySelector("#mobileNumber").value
            // console.log(mobileNo)
    let password = document.querySelector("#password1").value
        // console.log(password)
    let verifypassword = document.querySelector("#password2").value
          // console.log(verifypassword)

// information validation upon crating a new entry in the database. Lets create a new structure
// to check if passwords match
// to check if password are not empty
// to check the validation for mobile number what we can do is to check the length of the mobile number input
if((password !== '' && verifypassword !== '') && (verifypassword === password) && (mobileNo.length === 11 )){  
    fetch('https://fast-fjord-11707.herokuapp.com/api/users/email-exists',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email
        })
    }).then(res => res.json())
    .then(data => {
        if(data === false){
    fetch("https://fast-fjord-11707.herokuapp.com/api/users/register", {
       
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            firstName: fName,
            lastName: lName,
            email: email,
            mobileNo: mobileNo,
            password: password
        }) 
        // this section describe the body of the request converted in JSOn Format.
    }).then(res =>{
        return res.json() 
    }).then(data => {
        console.log(data)
        if (data === true) {
             Swal.fire({
                icon:'success',
                iconColor: 'green',
                title: 'Amazing',
                text: "New account registered successfully.", 
                background: '#ffe600f6',
                showConfirmButton: false,
                timer: 2500

            })
            window.location.replace('./login.html')
           
        }else {
            alert("Something went wrong in the registration.")
        }
    })
        } else{
            Swal.fire({
                icon: 'warning',
                iconColor: 'orange',
                title: 'Looks like you have doppelganger!',
                text: 'Email already exists, select another email.',
                background: '#ffe600f6'
    }); 
        }
    })
}else{
    Swal.fire({
        icon: 'error',
        iconColor: 'red',
        title: 'Oooops, take it easy!!',
        text: 'Finish the form first before clicking register.',
        background: '#ffe600f6'
    });  
    }
})
