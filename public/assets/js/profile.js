let token = localStorage.getItem('token')
let profile = document.querySelector('#profileContainer')

if(token === null | !token ){
	alert('You must login first');
	window.location.href='./login.html'
}else{
	fetch(`https://fast-fjord-11707.herokuapp.com/api/users/details`, {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.json())
	.then(data =>{
		console.log(data);

		let enrollmentData = data.enrollments.map(classData => {
			console.log(classData)
			return (
				`
				<tr>
					<td>${classData.courseName}</td>
					<td>${classData.enrolledOn}</td>
					<td>${classData.status}</td>
				</tr>
				`
				)
		}).join("")
		profile.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name:${data.firstName}</h3>
					<h3 class="text-center">Last Name:${data.lastName}</h3>
					<h3 class="text-center">Email:${data.email}</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Course Name:</th>
								<th>Enrolled On:</th>
								<th>Status:</th>
								<tbody>${enrollmentData}</tbody>
							</tr>
						</thead>
					</table>
				</section>
			</div>
		`
	})
}