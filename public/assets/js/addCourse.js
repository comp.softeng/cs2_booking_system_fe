
let createCourse = document.querySelector("#createCourse");

createCourse.addEventListener("submit", (event) =>{

    event.preventDefault();
    // lets capture values inside the input field
    let cName = document.querySelector("#courseName").value

    let cPrice = document.querySelector("#coursePrice").value
            
    let cDescription = document.querySelector("#courseDescription").value

    if((cName !== "" && cPrice !== "" && cDescription !== "")){
        fetch('https://fast-fjord-11707.herokuapp.com/api/course/course-exists',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: cName
        })
    }).then(res => res.json())
    .then(data => {
        if(data === false){
            fetch("https://fast-fjord-11707.herokuapp.com/api/course/addCourse", {
       
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: cName,
            price: cPrice,
            description: cDescription
        }) 
        // this section describe the body of the request converted in JSOn Format.
    }).then(res =>{
        return res.json() 
    }).then(data => {
        console.log(data)
        if (data === true) {
            Swal.fire({
                icon:'success',
                iconColor: 'green',
                title: 'Great Job',
                text: "New course added successfully.", 
                background: '#ffe600f6'
            })
        }else {
            Swal.fire({
                icon:'error',
                iconColor: 'red',
                title: 'Awww, Snap',
                text: "Something went wrong in adding new course..",
                background: '#ffe600f6'
            })
        }
    })    
    } else {
        Swal.fire({
                icon:'warning',
                iconColor: 'orange',
                title: 'Ooooops!',
                text: "Course name already exists..", 
                background: '#ffe600f6'
            })
    }   
})
    }else{
        Swal.fire({
                icon:'warning',
                title: 'Awwww!',
                text: "Please fill all the input fields", 
                background: '#ffe600f6'
            })
    }
})

    

// Advance Task Answer key:
// const formSubmit = document.querySelector('#createCourse')
// // 'submit'= button type
// formSubmit.addEventListener('submit', (event) => {
//     event.preventDefault()
//     // preventDefault() => w/c will be used to avoid automatic page redirection.

//     // lets capture all the information inside our input fields
//     // .value => describes the value attribute of the HTML element.
// let name = document.querySelector('#courseName').value
// // console.log(name)
// let cost = document.querySelector('#coursePrice').value
// // console.log(price)
// let description= document.querySelector('#courseDescription').value
// console.log(description)

// lets save a new entry inside the database, by describing the request method and overall structure
// fetch params: 
// // 1param = routes kung saan ipapadal aang request
// // 2param = form body structure

// fetch(' http://localhost:4000/api/course/addCourse', {
 
// // header section
//     method: 'POST',
//     headers: {
//         'Content-Type' : 'application/json'
//     },

//     // body section 
//     // convert it to JSON Format
//     body: JSON.stringify({
//         name: name ,
//         price: cost,
//         description: description
//     })
// }).then(res => {
//     return res.json()
// // after describing the structure of the request body, now create the structure of the response coming from the backend.
// }).then(info => {
//     if(info === true){
//         alert('A new course is succesfully created')
//     }else{
//         alert('Something wnet wrong adding a new course')
//     }
// })

