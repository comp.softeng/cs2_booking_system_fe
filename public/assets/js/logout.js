// we want to clear all the data inside our localStorage so that the session of the user will end.
// line 3=> 'clear()' will allow you to remove the contents of the storage object. Once cleared it will redirect the user to the login page ,just incase a new user wants to log in 
localStorage.clear()
window.location.replace('./login.html');