// console.log('Hello')
// console.log(window.location.search)

let params = new URLSearchParams(window.location.search)

// let id = params.get('courseId')
// console.log(...id)

// console.log(params.has('courseId'))

// console.log(params.get('courseId'))
let courseId = params.get('courseId')
// console.log(courseId)

let token = localStorage.getItem('token')

fetch(`https://fast-fjord-11707.herokuapp.com/api/course/${courseId}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${token}`
    }
  }).then(res => {
             return res.json()
          }).then(data => {
    if(data === true){
        Swal.fire({
            icon: 'success',
            iconColor: 'green',
            title: 'Succesfully Disabled Course!!',
            background: '#ffe600f6',
            showConfirmButton: false,
            timer: 2000
    }).then(() => {window.location.replace("./courses.html")})
    }
    else{
        //error in creating course, redirect to error page
        alert('something went wrong')
    }
})
