// console.log('hello')

// capture the html body which will display the content coming from the db.
let modalButton = document.querySelector('#adminButton')
// CAPTURE the html bofu which will display the content coming from the db
let isAdmin = localStorage.getItem("isAdmin")
let containerC = document.querySelector('#coursesContainer')
let cardFooter;
// 
if (isAdmin == "false" || !isAdmin) {
// id a user is a regular user, do not show the add course button
modalButton.innerHTML = null;
} else {
modalButton.innerHTML = `
 <div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
     </div>
`
}

let container = document.querySelector('#coursesContainer')
fetch('https://fast-fjord-11707.herokuapp.com/api/course/').then(res=> res.json()).then(data=>{
	// console.log(data);
// declare a variable that will display a result in the browser depending on the return
let courseData;
// create a control structure that will determin the value that the variable will hold
	if (data.length < 1) {
		courseData = "No Course Available"
	}else {
// we will iterate the courses collection and display each course inside the browser
	courseData = data.map(course => {
// lets check the make up of each element inside the courses collection
	// console.log(course._id);
// determine the display of the course if its a regular user(display the enroll button and display course button) or a admin
if(isAdmin == 'false' || !isAdmin){
	cardFooter = `<a href="./course.html?courseId=${course._id}"class="btn btn-warning text-white btn-block">View course details</a>

		`
}else{
	cardFooter = `<a href="./editCourse.html?courseId=${course._id}"class="btn btn-warning text-white btn-block">Edit Course</a>
	<a href="./deleteCourse.html?courseId=${course._id}"class="btn btn-warning text-white btn-block">Disable Course</a>`
}
		return(
			`<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${course.name}</h5>
						<p class="card-text text-left">
						${course.description}</p>
						<p class="card-text text-left">
						${course.price}</p>
						<p class="card-text text-left">
						${course.createdOn}</p>

					</div>
					<div class="card-footer">
						${cardFooter}
					</div>
				</div>
			</div>`
			// we attached a query string in the URL w/c allows us to embed the ID from the database record into the query string.
			//  ? => inside the url "acts" as a "separator", it indicates the end of a url resourcee path, and indicates the start of the query string.
			// # => this was originally 'used' to jump to an specific element with the same 'id name' or 'value'.
		)
		// we used 'join()'' method to create of a new string, it 'concatenated' all the objects inside the array and convert it each to a string data type
	}).join('')
}
	container.innerHTML = courseData
})